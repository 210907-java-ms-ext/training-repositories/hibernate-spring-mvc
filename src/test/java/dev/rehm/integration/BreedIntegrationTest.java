package dev.rehm.integration;

import dev.rehm.models.Breed;
import dev.rehm.models.Size;
import org.junit.jupiter.api.Test;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class BreedIntegrationTest {

    RestTemplate restTemplate = new RestTemplate();

    @Test
    public void testGetAllBreeds(){
        ResponseEntity<List> response = restTemplate.getForEntity("http://localhost:8082/hibernate-spring-mvc/breeds",
                List.class);
        assertAll(
                ()->assertTrue(response.getBody().size()>0),
                ()->assertEquals(response.getStatusCode(), HttpStatus.OK)
        );
    }

    @Test
    public void testUnauthorized401(){
        Breed newBreed = new Breed("Dachshund",16, Size.SMALL);
        assertThrows(HttpClientErrorException.class,
                ()->restTemplate.postForEntity("http://localhost:8082/hibernate-spring-mvc/breeds", newBreed, Breed.class));
    }

    @Test
    public void testAuthorizedAddsBreed(){
        Breed newBreed = new Breed("Dachshund",16, Size.SMALL);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "supersecrettoken");
        HttpEntity<Breed> entity = new HttpEntity<>(newBreed, headers);

        ResponseEntity<Breed> response = restTemplate.exchange("http://localhost:8082/hibernate-spring-mvc/breeds",
                HttpMethod.POST,
                entity,
                Breed.class);

        assertAll(
                ()->assertEquals(HttpStatus.CREATED,response.getStatusCode()),
                ()->assertNotNull(response.getBody()),
                ()->assertTrue(response.getBody().getId()!=0)
        );
    }

}
