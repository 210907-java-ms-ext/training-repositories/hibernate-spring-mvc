package dev.rehm.dao;

import dev.rehm.models.Breed;
import dev.rehm.models.Dog;
import dev.rehm.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class DogDAOImpl implements DogDAO {
    @Override
    public List<Dog> getAllDogs() {
        try(Session s = HibernateUtil.getSession()){
           return s.createQuery("from Dog", Dog.class).list();
        }
    }

    @Override
    public Dog getDogById(int id) {
        try(Session s = HibernateUtil.getSession()){
            return s.get(Dog.class, id);
        }
    }

    @Override
    public Dog addDog(Dog dog) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            int id = (int) s.save(dog);
            tx.commit();
            dog.setId(id);
            return dog;
        }
    }

    @Override
    public List<Dog> getDogsByBreedName(String breed) {
        try(Session s = HibernateUtil.getSession()) {
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<Dog> cq = cb.createQuery(Dog.class);

            Root<Dog> root = cq.from(Dog.class);
            cq.select(root);

            cq.where(cb.equal(root.get("breed").get("name"),breed));
            Query<Dog> q = s.createQuery(cq);
            return q.list();
        }
    }

    @Override
    public List<Dog> getDogsByBreedNameAndName(String name, String breed) {
        try(Session s = HibernateUtil.getSession()) {
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<Dog> cq = cb.createQuery(Dog.class);

            Root<Dog> root = cq.from(Dog.class);
            cq.select(root);

            cq.where(cb.and(
                    cb.equal(root.get("breed").get("name"),breed),
                    cb.equal(root.get("name"),name)));
            Query<Dog> q = s.createQuery(cq);
            return q.list();
        }
    }

    @Override
    public List<Dog> getDogsByName(String name) {
        try(Session s = HibernateUtil.getSession()) {
            Query<Dog> itemQuery = s.createQuery("from Dog where name = :nameParam", Dog.class);
            itemQuery.setParameter("nameParam", name);
            return itemQuery.list();
        }
    }
}
