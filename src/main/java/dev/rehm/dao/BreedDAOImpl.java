package dev.rehm.dao;

import dev.rehm.models.Breed;
import dev.rehm.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class BreedDAOImpl implements BreedDAO {

    @Override
    public Breed addBreed(Breed breed) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            int id = (int) s.save(breed);
            tx.commit();
            breed.setId(id);
            return breed;
        }
    }

    @Override
    public List<Breed> getAll() {
        try(Session s = HibernateUtil.getSession()){
            return s.createQuery("from Breed", Breed.class).list();
        }
    }
}
