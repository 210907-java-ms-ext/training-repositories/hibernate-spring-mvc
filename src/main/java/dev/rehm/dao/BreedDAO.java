package dev.rehm.dao;

import dev.rehm.models.Breed;

import java.util.List;

public interface BreedDAO {

    public Breed addBreed(Breed breed);
    public List<Breed> getAll();
}
