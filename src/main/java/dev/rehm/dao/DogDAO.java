package dev.rehm.dao;

import dev.rehm.models.Dog;

import java.util.List;

public interface DogDAO {

    List<Dog> getAllDogs();
    Dog getDogById(int id);
    Dog addDog(Dog dog);
    List<Dog> getDogsByBreedName(String breed);
    List<Dog> getDogsByBreedNameAndName(String name, String breed);
    List<Dog> getDogsByName(String name);

}
