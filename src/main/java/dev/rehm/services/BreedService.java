package dev.rehm.services;

import dev.rehm.models.Breed;

import java.util.List;

public interface BreedService {

    public Breed addBreed(Breed breed);
    public List<Breed> getAll();
}
