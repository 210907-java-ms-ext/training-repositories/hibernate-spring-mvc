package dev.rehm.services;

import dev.rehm.dao.DogDAO;
import dev.rehm.models.Breed;
import dev.rehm.models.Dog;
import dev.rehm.models.Size;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class DogServiceImpl implements DogService {

    @Autowired
    private DogDAO dogDAO;


    @Override
    public List<Dog> getAllDogs() {
        return dogDAO.getAllDogs();
    }

    @Override
    public Dog getDogById(int id) {
        return dogDAO.getDogById(id);
    }

    @Override
    public List<Dog> getDogsByBreedName(String breedName) {
        return dogDAO.getDogsByBreedName(breedName);
    }

    @Override
    public List<Dog> getDogsByName(String name) {
        return dogDAO.getDogsByName(name);
    }

    @Override
    public List<Dog> getDogsByNameAndBreed(String name, String breed) {
        return dogDAO.getDogsByBreedNameAndName(name, breed);
    }

    @Override
    public Dog createNewDog(Dog newDog) {
        return dogDAO.addDog(newDog);
    }

}
