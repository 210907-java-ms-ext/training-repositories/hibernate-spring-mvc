package dev.rehm.services;

import dev.rehm.dao.BreedDAO;
import dev.rehm.models.Breed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BreedServiceImpl implements BreedService{

    @Autowired
    private BreedDAO breedDao;

    @Override
    public Breed addBreed(Breed breed) {
        return breedDao.addBreed(breed);
    }

    @Override
    public List<Breed> getAll() {
        return breedDao.getAll();
    }
}
