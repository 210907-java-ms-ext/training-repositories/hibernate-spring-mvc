package dev.rehm.services;

import dev.rehm.models.Breed;
import dev.rehm.models.Dog;
import dev.rehm.models.Size;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class DogServiceListImpl implements DogService {

    private List<Dog> dogs = new ArrayList<>();

    public DogServiceListImpl(){
        super();
        dogs.add(new Dog(1, "White Fang",new Breed(3, "Siberian Husky", 13, Size.LARGE)));
        dogs.add(new Dog(2, "Apollo",new Breed(4, "Pitbull", 15, Size.LARGE)));
        dogs.add(new Dog(3, "Lear", new Breed(5, "Golden Retriever",12, Size.LARGE)));
        dogs.add(new Dog(4, "Chowder", new Breed(6, "Chow Chow",15, Size.MEDIUM)));
        dogs.add(new Dog(5, "Amanda", new Breed(5, "Golden Retriever",12, Size.LARGE)));
        dogs.add(new Dog(6, "Apollo", new Breed(5, "Golden Retriever",12, Size.LARGE)));
//        dogs.add(null);
//        dogs.add(new Dog(6, "Apollo", null));


    }


    @Override
    public List<Dog> getAllDogs() {
        return new ArrayList<>(dogs);
    }

    @Override
    public Dog getDogById(int id) {
        return dogs.stream()
                .filter(Objects::nonNull)
                .filter(d->d.getId()==id)
                .findAny().orElse(null);
    }

    @Override
    public List<Dog> getDogsByBreedName(String breedName) {
        return dogs.stream()
                .filter(Objects::nonNull)
                .filter(d->d.getBreed()!=null && d.getBreed().getName().equals(breedName))
                .collect(Collectors.toList());
    }

    @Override
    public List<Dog> getDogsByName(String name) {
        return dogs.stream()
                .filter(Objects::nonNull)
                .filter(d->d.getName()!=null && d.getName().equals(name))
                .collect(Collectors.toList());
    }

    @Override
    public List<Dog> getDogsByNameAndBreed(String name, String breed) {
        return dogs.stream()
                .filter(Objects::nonNull)
                .filter(d->d.getBreed()!=null && d.getBreed().getName().equals(breed) && d.getName()!=null && d.getName().equals(name))
                .collect(Collectors.toList());
    }

    @Override
    public Dog createNewDog(Dog newDog) {
        dogs.add(newDog);
        return newDog;
    }

    public Dog updateDog(Dog updatedDog, int id) {
        for(int i=0; i<dogs.size(); i++){
            Dog currentDog = dogs.get(i);
            if(currentDog!=null && currentDog.getId()==id){
                dogs.set(i,updatedDog);
                return updatedDog;
            }
        }
        return null;
    }

    public void deleteDog(int id) {
        dogs.removeIf(nextDog -> nextDog != null && nextDog.getId() == id);
    }

}
