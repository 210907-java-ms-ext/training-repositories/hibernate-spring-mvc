package dev.rehm.services;

import dev.rehm.models.Dog;

import java.util.List;

public interface DogService {

    public List<Dog> getAllDogs();
    public Dog getDogById(int id);
    public List<Dog> getDogsByBreedName(String breedName);
    public List<Dog> getDogsByName(String name);
    public List<Dog> getDogsByNameAndBreed(String name, String breed);
    public Dog createNewDog(Dog newDog);

}
