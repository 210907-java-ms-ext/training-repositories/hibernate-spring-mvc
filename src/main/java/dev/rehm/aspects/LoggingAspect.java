package dev.rehm.aspects;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class LoggingAspect {

    private Logger logger = Logger.getLogger(LoggingAspect.class);

    @AfterReturning("within(dev.rehm.controllers.*)")
    public void logMethodSuccess(JoinPoint jp){
        logger.info(jp.getSignature().getName() + " invoked successfully");
    }

    @AfterThrowing("within(dev.rehm.controllers.*)")
    public void logMethodFailure(JoinPoint jp){
        logger.warn("there was an exception thrown while trying to execute "+ jp.getSignature().getName());
    }

    @After("within(dev.rehm.controllers.*)")
    public void logRequest(){
          HttpServletRequest request =
                  ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();
          logger.info(request.getMethod()+ " request made to "+ request.getRequestURI());

    }



}
