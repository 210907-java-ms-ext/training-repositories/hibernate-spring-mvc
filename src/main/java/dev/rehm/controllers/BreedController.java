package dev.rehm.controllers;


import dev.rehm.models.Breed;
import dev.rehm.services.BreedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController // @Controller (registers class as a bean) + @ResponseBody implied on each method
@CrossOrigin
@RequestMapping("/breeds")
public class BreedController {

    @Autowired
    private BreedService breedService;

    @PostMapping
    public ResponseEntity<Breed> addNewBreed(@RequestBody Breed breed){
        return new ResponseEntity<>(breedService.addBreed(breed), HttpStatus.CREATED);
    }

    @GetMapping
    public List<Breed> getAllBreeds(){
        return breedService.getAll();
    }


}
